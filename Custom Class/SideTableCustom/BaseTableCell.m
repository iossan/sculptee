//
//  BaseTableCell.m
//  E2MRichmond
//
//  Created by Satish Kumar on 1/19/12.
//  Copyright 2012 Web Spiders (India) Pvt. Ltd. - http://www.webspiders.com. All rights reserved.
//

#import "BaseTableCell.h"


@implementation BaseTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}

+ (BaseTableCell *)cellFromNibNamed:(NSString *)nibName {
	NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:nibName owner:self options:NULL];
	NSEnumerator *nibEnumerator = [nibContents objectEnumerator];
	BaseTableCell *customCell = nil;
	NSObject* nibItem = nil;
	while ((nibItem = [nibEnumerator nextObject]) != nil) {
		if ([nibItem isKindOfClass:[BaseTableCell class]]) {
			customCell = (BaseTableCell *)nibItem;
			break; // we have a winner
		}
	}
	return customCell;
}


@end
