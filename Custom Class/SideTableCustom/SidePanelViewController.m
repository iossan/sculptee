//
//  SidePanelViewController.m
//  HSComply
//
//  Created by Santanu Mondal on 09/06/17.
//  Copyright © 2017 Santanu Mondal. All rights reserved.
//

#import "SidePanelViewController.h"

@interface SidePanelViewController ()
@end

@implementation SidePanelViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnItemAction:(UIButton *)sender {
    [self.menuController dismissAnimated:YES];
}

@end
