//
//  BankListTableCell.h
//  Omlis
//
//  Created by enduetech on 16/04/13.
//  Copyright (c) 2013 Enduetech. All rights reserved.
//

#import "BaseTableCell.h"

@interface BankListTableCell : BaseTableCell
@property (retain, nonatomic) IBOutlet UIImageView *imgVBanklogo;
@property (retain, nonatomic) IBOutlet UILabel *lblBankName;
@property (retain, nonatomic) IBOutlet UILabel *lblAccType;
@property (retain, nonatomic) IBOutlet UILabel *lblBalence;
@property (retain, nonatomic) IBOutlet UIImageView *imgVDiscloserBtn;

@end
