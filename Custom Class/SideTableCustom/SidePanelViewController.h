//
//  SidePanelViewController.h
//  HSComply
//
//  Created by Santanu Mondal on 09/06/17.
//  Copyright © 2017 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBMenuController.h"

@interface SidePanelViewController : UIViewController
@property (nonatomic, weak) SBMenuController *menuController;

@end
