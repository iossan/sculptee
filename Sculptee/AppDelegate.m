//
//  AppDelegate.m
//  Sculptee
//
//  Created by Santanu Mondal on 25/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GoogleSignIn/GoogleSignIn.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
static NSString * const kClientID =
@"185478403462-2bvloki432n45c6sh1mi9evs588v16r9.apps.googleusercontent.com";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [GIDSignIn sharedInstance].clientID = kClientID;
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"signinhome"];
    _navigationController=[[SBLeftRightMenuNavigationController alloc]initWithRootViewController:lvc];
    _navigationController.navigationBarHidden = YES;
    self.window=[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.rootViewController = _navigationController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    return( [[FBSDKApplicationDelegate sharedInstance] application:app
                                                           openURL:url
                                                 sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                        annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]
           ||
           
           [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationLaunchOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationLaunchOptionsAnnotationKey]]
           );
    
    
    
    
    //    return [[GIDSignIn sharedInstance] handleURL:url
    //                               sourceApplication:options[UIApplicationLaunchOptionsSourceApplicationKey]
    //                                      annotation:options[UIApplicationLaunchOptionsAnnotationKey]];
    //    ||
    //
    //     [[FBSDKApplicationDelegate sharedInstance] application:application
    //                                                          openURL:urll
    //                                                sourceApplication:optionss[UIApplicationOpenURLOptionsSourceApplicationKey]
    //                                                       annotation:optionss[UIApplicationOpenURLOptionsAnnotationKey]];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSDKAppEvents activateApp];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
