//
//  AppDelegate.h
//  Sculptee
//
//  Created by Santanu Mondal on 25/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBLeftRightMenuNavigationController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (strong, nonatomic) SBLeftRightMenuNavigationController *navigationController;
@property (strong, nonatomic) UIWindow *window;


@end

