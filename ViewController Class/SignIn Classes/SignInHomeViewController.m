//
//  SignInHomeViewController.m
//  Sculptee
//
//  Created by Santanu Mondal on 25/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "SignInHomeViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "GoogleSignIn/GoogleSignIn.h"

@interface SignInHomeViewController ()<GIDSignInDelegate,GIDSignInUIDelegate>

@end

@implementation SignInHomeViewController
static NSString * const kClientID =
@"185478403462-2bvloki432n45c6sh1mi9evs588v16r9.apps.googleusercontent.com";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [GIDSignInButton class];
    
    //[[GIDSignIn sharedInstance] setAllowsSignInWithWebView:NO];
    
    GIDSignIn *signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.delegate = self;
    signIn.uiDelegate = self;
}

- (IBAction)clickLogin:(id)sender {
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SignIn" bundle:nil];
//    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"signin"];
//    
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.8;
//    transition.type = kCATransitionReveal;
//    transition.subtype = kCATransitionFade;
//    
//    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
//    [self.navigationController pushViewController:lvc animated:YES];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickSignUp:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"SignUp" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"signup"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickGooglePlus:(id)sender {
    GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
    [sigNIn setDelegate:self];
    [sigNIn setUiDelegate:self];
    sigNIn.shouldFetchBasicProfile = YES;
    //sigNIn.allowsSignInWithBrowser = NO;
    //sigNIn.allowsSignInWithWebView = YES;
    sigNIn.scopes = @[@"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/userinfo.email",@"https://www.googleapis.com/auth/userinfo.profile"];
    sigNIn.clientID =kClientID;
    [sigNIn signIn];
}

#pragma mark - GIDSignInDelegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error
{
    if (error) {
        return;
    }
    
    NSLog(@"%@",[GIDSignIn sharedInstance].currentUser.profile.email);
    NSLog(@"%@",[GIDSignIn sharedInstance].currentUser.profile.name);
    
    //[self socialNetWorkLoginGooglePlus:[GIDSignIn sharedInstance].currentUser.profile.email UserName:[GIDSignIn sharedInstance].currentUser.profile.name];
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
}

- (IBAction)clickFacebook:(id)sender {
    /*********  logout the current session ************/
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [FBSDKProfile setCurrentProfile:nil];
    /*********  logout the current session ************/
    
    /*********  start the new session for login ************/
    
    // FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login logInWithReadPermissions:@[@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            // Process error
        } else if (result.isCancelled) {
            // Handle cancellations
        }
        else {
            if ([result.grantedPermissions containsObject:@"email"]) {
                NSLog(@"Logged in");
                if ([FBSDKAccessToken currentAccessToken])
                {
                    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                    [parameters setValue:@"id,name,email" forKey:@"fields"];
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id user, NSError *error)
                     {
                         if (!error)
                         {
                             self->dictUser = (NSDictionary *)user;
                             
                             NSLog(@"%@",self->dictUser);
                             //[self socialFBNetWorkLogin];
                         }
                     }];
                }
            }
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
