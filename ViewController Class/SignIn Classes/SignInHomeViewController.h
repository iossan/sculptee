//
//  SignInHomeViewController.h
//  Sculptee
//
//  Created by Santanu Mondal on 25/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoogleSignIn/GoogleSignIn.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignInHomeViewController : UIViewController<GIDSignInDelegate>{
    NSDictionary *dictUser;
}
- (IBAction)clickLogin:(id)sender;
- (IBAction)clickSignUp:(id)sender;
- (IBAction)clickGooglePlus:(id)sender;
- (IBAction)clickFacebook:(id)sender;

@end

NS_ASSUME_NONNULL_END
