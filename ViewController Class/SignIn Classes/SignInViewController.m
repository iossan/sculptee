//
//  SignInViewController.m
//  Sculptee
//
//  Created by Santanu Mondal on 25/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "SignInViewController.h"

@interface SignInViewController ()

@end

@implementation SignInViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIColor *color = [UIColor whiteColor];
    self.txtPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    self.txtMobNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [self.view addGestureRecognizer:singleTap];
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture{
    [self.txtPassword resignFirstResponder];
    [self.txtMobNumber resignFirstResponder];
}

- (IBAction)clickLogin:(id)sender {
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    UIViewController *lvc = [sb instantiateViewControllerWithIdentifier:@"dashboard"];
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.8;
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFade;
    
    [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [self.navigationController pushViewController:lvc animated:YES];
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TextField Delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField==self.txtMobNumber){
        [self.txtPassword becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder] ;
    }
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
