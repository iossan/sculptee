//
//  SignInViewController.h
//  Sculptee
//
//  Created by Santanu Mondal on 25/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SignInViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtMobNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)clickLogin:(id)sender;
- (IBAction)clickBack:(id)sender;

@end

NS_ASSUME_NONNULL_END
