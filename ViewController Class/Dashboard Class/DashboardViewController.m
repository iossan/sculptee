//
//  DashboardViewController.m
//  Sculptee
//
//  Created by Santanu Mondal on 26/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "DashboardViewController.h"
#import "SBLeftRightMenuNavigationController.h"
#import "CakeDetailsListViewController.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController
@synthesize arrCategory;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.arrCategory = [NSMutableArray array];
    
    [self loadCategory];
}

-(void)loadCategory{
    NSString* strURL = [NSString stringWithFormat:@"%@",CATEGORY_LIST];
    NSLog(@"%@",strURL);
    
    AFOAuth1Client * client = [[AFOAuth1Client alloc] initWithBaseURL:[NSURL URLWithString:strURL] key:@"ck_f36a691ba7ab086bda16115b5901390a7dc04758" secret:@"cs_32fea993cccb0e6a39a05a82629b493ecc22a389"];
    [client setSignatureMethod:AFHMACSHA1SignatureMethod];
    [client setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    
    NSMutableURLRequest * request =[client requestWithMethod:@"GET" path:strURL parameters:nil];
    //here path is actually the name of the method
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [client registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Success Print the response body in text
        NSString *resp = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        self.arrCategory = [resp JSONValue];
        NSLog(@"%@", self.arrCategory);
        
        [self addButtonOnScrollView];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Something Went wrong..
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

-(void)addButtonOnScrollView{
    for (UIView *v in self.scrollViewMain.subviews) {
        if ([v isKindOfClass:[UIView class]]) {
            [v removeFromSuperview];
        }
    }
    
    CGFloat viewWidth = 0.0;
    CGFloat viewHeight = 0.0;
    
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.width == 320){
        viewWidth = 25.0;
        viewHeight = 30.0;
    }
    else if(result.width == 375){
        viewWidth = 30.0;
        viewHeight = 30.0;
    }
    
    for(int i=0; i<[self.arrCategory count]; i++){
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            if(result.width == 320){
                UIView *vwCategory = [[UIView alloc] init];
                vwCategory.frame = CGRectMake(viewWidth, viewHeight, 120, 120);
                vwCategory.backgroundColor = [UIColor clearColor];
                [self.scrollViewMain addSubview:vwCategory];
                
                UIImageView *imgCateBack = [[UIImageView alloc] init];
                imgCateBack.frame = CGRectMake(0, 0, 120, 120);
                imgCateBack.image = [UIImage imageNamed:@"cateBack"];
                [vwCategory addSubview:imgCateBack];
                
                UIImageView *imgCateIcon = [[UIImageView alloc] init];
                imgCateIcon.frame = CGRectMake(27.5, 10, 65, 65);
                imgCateIcon.imageURL = [NSURL URLWithString:[[[self.arrCategory objectAtIndex:i] objectForKey:@"image"] objectForKey:@"src"]];
                imgCateIcon.contentMode = UIViewContentModeScaleAspectFit;
                [vwCategory addSubview:imgCateIcon];
                
                UILabel *lblCatName = [[UILabel alloc] init];
                lblCatName.frame = CGRectMake(0, 80, 120, 20);
                lblCatName.text = [[self.arrCategory objectAtIndex:i] objectForKey:@"name"];
                lblCatName.textAlignment = NSTextAlignmentCenter;
                lblCatName.textColor = [UIColor colorWithRed:62.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
                lblCatName.font = [UIFont fontWithName:@"MavenProRegular" size:14.0];
                [vwCategory addSubview:lblCatName];
                
                UIButton *btnCateDesc = [UIButton buttonWithType:UIButtonTypeCustom];
                btnCateDesc.frame = CGRectMake(0, 0, 120, 120);
                btnCateDesc.tag = i;
                [btnCateDesc addTarget:self action:@selector(clickCategoryDetails:) forControlEvents:UIControlEventTouchUpInside];
                [vwCategory addSubview:btnCateDesc];
                
                viewWidth = viewWidth + 150;
                
                if(viewWidth == 325){
                    viewWidth = 25.0;
                    viewHeight = viewHeight + 150;
                }
            }
            else if(result.width == 375){
                UIView *vwCategory = [[UIView alloc] init];
                vwCategory.frame = CGRectMake(viewWidth, viewHeight, 140, 140);
                vwCategory.backgroundColor = [UIColor clearColor];
                [self.scrollViewMain addSubview:vwCategory];
                
                UIImageView *imgCateBack = [[UIImageView alloc] init];
                imgCateBack.frame = CGRectMake(0, 0, 140, 140);
                imgCateBack.image = [UIImage imageNamed:@"cateBackBig"];
                [vwCategory addSubview:imgCateBack];
                
                UIImageView *imgCateIcon = [[UIImageView alloc] init];
                imgCateIcon.frame = CGRectMake(37.5, 15, 65, 65);
                imgCateIcon.imageURL = [NSURL URLWithString:[[[self.arrCategory objectAtIndex:i] objectForKey:@"image"] objectForKey:@"src"]];
                imgCateIcon.contentMode = UIViewContentModeScaleAspectFit;
                [vwCategory addSubview:imgCateIcon];
                
                UILabel *lblCatName = [[UILabel alloc] init];
                lblCatName.frame = CGRectMake(0, 95, 140, 20);
                lblCatName.text = [[self.arrCategory objectAtIndex:i] objectForKey:@"name"];
                lblCatName.textAlignment = NSTextAlignmentCenter;
                lblCatName.textColor = [UIColor colorWithRed:62.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
                lblCatName.font = [UIFont fontWithName:@"MavenProRegular" size:14.0];
                [vwCategory addSubview:lblCatName];
                
                UIButton *btnCateDesc = [UIButton buttonWithType:UIButtonTypeCustom];
                btnCateDesc.frame = CGRectMake(0, 0, 140, 140);
                btnCateDesc.tag = i;
                [btnCateDesc addTarget:self action:@selector(clickCategoryDetails:) forControlEvents:UIControlEventTouchUpInside];
                [vwCategory addSubview:btnCateDesc];
                
                viewWidth = viewWidth + 175;
                
                if(viewWidth == 380){
                    viewWidth = 30.0;
                    viewHeight = viewHeight + 170;
                }
            }
        }
        else{
            // iPad Design
        }
    }
}

-(void)clickCategoryDetails:(id)sender{
    UIButton *btn =(UIButton*)sender;
    
    CakeDetailsListViewController *obj_CakeDetails;
    obj_CakeDetails = [[CakeDetailsListViewController alloc] initWithNibName:@"CakeDetailsListViewController" bundle:nil];
    obj_CakeDetails.strHeading = [[self.arrCategory objectAtIndex:btn.tag] objectForKey:@"name"];
    obj_CakeDetails.strCategoryID = [NSString stringWithFormat:@"%@",[[self.arrCategory objectAtIndex:btn.tag] objectForKey:@"id"]];
    [self.navigationController pushViewController:obj_CakeDetails animated:YES];
}

- (IBAction)clickSidePanel:(id)sender {
    //SBLeftRightMenuNavigationController *nav = (SBLeftRightMenuNavigationController *)self.navigationController;
    //[nav showLeftMenuAnimated:YES];
}

- (IBAction)clickNotification:(id)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
