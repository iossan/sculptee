//
//  DashboardViewController.h
//  Sculptee
//
//  Created by Santanu Mondal on 26/03/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardViewController : UIViewController
- (IBAction)clickSidePanel:(id)sender;
- (IBAction)clickNotification:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewMain;
@property (nonatomic, retain) NSMutableArray *arrCategory;

@end

NS_ASSUME_NONNULL_END
