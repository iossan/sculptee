//
//  CakeDetailsListViewController.m
//  Sculptee
//
//  Created by Santanu Mondal on 15/07/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "CakeDetailsListViewController.h"
#import "CakeListTableViewCell.h"
#import "AsyncImageView.h"
#import "CakeDetailsPageViewController.h"

@interface CakeDetailsListViewController ()

@end

@implementation CakeDetailsListViewController
@synthesize strHeading,strCategoryID,arrCategoryList;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.arrCategoryList = [NSMutableArray array];
    
    self.lblHeading.text = self.strHeading;
    
    [self loadCategoryList];
}

-(void)loadCategoryList{
    NSString* strURL = [NSString stringWithFormat:@"%@",CATEGORY_PRODUCT_LIST];
    strURL = [strURL stringByAppendingString:self.strCategoryID];
    NSLog(@"%@",strURL);

    AFOAuth1Client * client = [[AFOAuth1Client alloc] initWithBaseURL:[NSURL URLWithString:strURL] key:@"ck_f36a691ba7ab086bda16115b5901390a7dc04758" secret:@"cs_32fea993cccb0e6a39a05a82629b493ecc22a389"];
    [client setSignatureMethod:AFHMACSHA1SignatureMethod];
    [client setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    
    NSMutableURLRequest * request =[client requestWithMethod:@"GET" path:strURL parameters:nil];
    //here path is actually the name of the method
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [client registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Success Print the response body in text
        NSString *resp = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        self.arrCategoryList = [resp JSONValue];
        NSLog(@"%@", self.arrCategoryList);
        
        if([self.arrCategoryList count]>0){
            self.tableViewCakeList.hidden = NO;
            [self.tableViewCakeList reloadData];
        }
        else{
            self.tableViewCakeList.hidden = YES;
            self.imgGredient.hidden = NO;
            self.lblTextComimgSoon.hidden = NO;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Something Went wrong..
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([self.arrCategoryList count]>0){
        return [self.arrCategoryList count];
    }
    else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self.arrCategoryList count]>0){
        return 250;
    }
    else{
        return 50;
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    CakeListTableViewCell *cell = (CakeListTableViewCell*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = (CakeListTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"CakeListTableViewCell" owner:nil options:nil] firstObject];
    }
    
    if([self.arrCategoryList count]>0){
        cell.lblCakeName.text = [[self.arrCategoryList objectAtIndex:indexPath.row] objectForKey:@"name"];
        cell.lblRationPoint.text = [[self.arrCategoryList objectAtIndex:indexPath.row] objectForKey:@"average_rating"];
        cell.lblRationPoint.text = [cell.lblRationPoint.text stringByAppendingString:@" *"];

        cell.lblReviewAndRating.text = [NSString stringWithFormat:@"%@",[[self.arrCategoryList objectAtIndex:indexPath.row] objectForKey:@"rating_count"]];
        cell.lblReviewAndRating.text = [cell.lblReviewAndRating.text stringByAppendingString:@" rating & "];
        cell.lblReviewAndRating.text = [cell.lblReviewAndRating.text stringByAppendingString:[NSString stringWithFormat:@"%@",[[self.arrCategoryList objectAtIndex:indexPath.row] objectForKey:@"product_reviews_count"]]];
        cell.lblReviewAndRating.text = [cell.lblReviewAndRating.text stringByAppendingString:@" review"];
        
        if([[self.arrCategoryList objectAtIndex:indexPath.row] objectForKey:@"stock_quantity"]>0){
            cell.lblAveNotAve.text = @"AVAILABLE";
            cell.lblAveNotAve.textColor = [UIColor greenColor];
        }
        else{
            cell.lblAveNotAve.text = @"NOT AVAILABLE";
            cell.lblAveNotAve.textColor = [UIColor colorWithRed:242.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        }
        
        cell.imgVwCoverCake.imageURL = [NSURL URLWithString:[[[[self.arrCategoryList objectAtIndex:indexPath.row] objectForKey:@"images"]objectAtIndex:0] objectForKey:@"src"]];

        [cell.btnCakeDetails addTarget:self action:@selector(clickSelectCake:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnCakeDetails.tag = indexPath.row;
    }
    else{
        
    }
    
    return cell;
}

-(void)clickSelectCake:(id)sender{
    UIButton *btn = (UIButton*)sender;
    
    CakeDetailsPageViewController *obj_CakeDetails;
    obj_CakeDetails = [[CakeDetailsPageViewController alloc] initWithNibName:@"CakeDetailsPageViewController" bundle:nil];
    obj_CakeDetails.strHeading = self.strHeading;
    //obj_CakeDetails.dictCakeDetails = [self.arrCategoryList objectAtIndex:btn.tag];
    obj_CakeDetails.strProductID = [NSString stringWithFormat:@"%@",[[self.arrCategoryList objectAtIndex:btn.tag] objectForKey:@"id"]];
    [self.navigationController pushViewController:obj_CakeDetails animated:YES];
}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
