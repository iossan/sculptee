//
//  CakeListTableViewCell.h
//  Sculptee
//
//  Created by Santanu Mondal on 15/07/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CakeListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgVwCoverCake;
@property (weak, nonatomic) IBOutlet UILabel *lblCakeName;
@property (weak, nonatomic) IBOutlet UILabel *lblRationPoint;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewAndRating;
@property (weak, nonatomic) IBOutlet UILabel *lblAveNotAve;
@property (weak, nonatomic) IBOutlet UILabel *lblCrossPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblCakePrice;
@property (weak, nonatomic) IBOutlet UIButton *btnCakeDetails;
@property (weak, nonatomic) IBOutlet UIView *viewLower;

@end

NS_ASSUME_NONNULL_END
