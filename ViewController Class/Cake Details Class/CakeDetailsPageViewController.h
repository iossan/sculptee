//
//  CakeDetailsPageViewController.h
//  Sculptee
//
//  Created by Santanu Mondal on 15/07/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CakeDetailsPageViewController : UIViewController
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (nonatomic, retain) NSString *strHeading;
@property (nonatomic, retain) NSDictionary *dictCakeDetails;
@property (nonatomic, retain) NSString *strProductID;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewMain;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwCoverCake;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwSmall1;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwSmall2;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwSmall3;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwSmall4;
@property (weak, nonatomic) IBOutlet UILabel *lblCakeName;
@property (weak, nonatomic) IBOutlet UILabel *lblTextAvaOrNotAva;
@property (weak, nonatomic) IBOutlet UILabel *lblCircleAva;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;
@property (weak, nonatomic) IBOutlet UILabel *lblReviewRating;
@property (weak, nonatomic) IBOutlet UILabel *lblOriginalPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblCrossPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblOffPercentage;
@property (weak, nonatomic) IBOutlet UIView *viewEggbased;
@property (weak, nonatomic) IBOutlet UIView *viewEggless;
@property (weak, nonatomic) IBOutlet UIView *viewFlavour;
@property (weak, nonatomic) IBOutlet UIView *viewWeight;
@property (weak, nonatomic) IBOutlet UITextView *txtVwCakeMsg;

@end

NS_ASSUME_NONNULL_END
