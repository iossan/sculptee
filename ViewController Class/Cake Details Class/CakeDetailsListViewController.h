//
//  CakeDetailsListViewController.h
//  Sculptee
//
//  Created by Santanu Mondal on 15/07/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CakeDetailsListViewController : UIViewController
- (IBAction)clickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblHeading;
@property (weak, nonatomic) IBOutlet UITableView *tableViewCakeList;
@property (weak, nonatomic) IBOutlet UIImageView *imgGredient;
@property (weak, nonatomic) IBOutlet UILabel *lblTextComimgSoon;

@property (nonatomic, retain) NSString *strHeading;
@property (nonatomic, retain) NSString *strCategoryID;
@property (nonatomic, retain) NSMutableArray *arrCategoryList;

@end

NS_ASSUME_NONNULL_END
