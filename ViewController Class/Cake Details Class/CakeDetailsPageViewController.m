//
//  CakeDetailsPageViewController.m
//  Sculptee
//
//  Created by Santanu Mondal on 15/07/19.
//  Copyright © 2019 Santanu Mondal. All rights reserved.
//

#import "CakeDetailsPageViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface CakeDetailsPageViewController ()

@end

@implementation CakeDetailsPageViewController
@synthesize strHeading,dictCakeDetails,strProductID;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.lblHeading.text = self.strHeading;
    
    [self.view addSubview:self.scrollViewMain];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.width == 320){
            self.scrollViewMain.frame = CGRectMake(0, 64, 320, 454);
        }
        else if(result.width == 375){
            self.scrollViewMain.frame = CGRectMake(0, 64, 375, 664);
        }
        else{
            self.scrollViewMain.frame = CGRectMake(0, 0, 414, 454);
        }
    }
    else{
        self.scrollViewMain.frame = CGRectMake(0, 0, 768, 865);
    }
    
    self.viewEggbased.layer.borderWidth = 1.0;
    self.viewEggbased.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.viewEggless.layer.borderWidth = 1.0;
    self.viewEggless.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.viewFlavour.layer.borderWidth = 1.0;
    self.viewFlavour.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.viewWeight.layer.borderWidth = 1.0;
    self.viewWeight.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.txtVwCakeMsg.layer.borderWidth = 1.0;
    self.txtVwCakeMsg.layer.borderColor = [UIColor blackColor].CGColor;
    
    [self loadProductDetailsAPI];
}

-(void)loadProductDetailsAPI{
    NSString* strURL = [NSString stringWithFormat:@"%@",SINGLE_PRODUCT_DETAILS];
    strURL = [strURL stringByAppendingString:self.strProductID];
    NSLog(@"%@",strURL);
    
    AFOAuth1Client * client = [[AFOAuth1Client alloc] initWithBaseURL:[NSURL URLWithString:strURL] key:@"ck_f36a691ba7ab086bda16115b5901390a7dc04758" secret:@"cs_32fea993cccb0e6a39a05a82629b493ecc22a389"];
    [client setSignatureMethod:AFHMACSHA1SignatureMethod];
    [client setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded"];
    
    NSMutableURLRequest * request =[client requestWithMethod:@"GET" path:strURL parameters:nil];
    //here path is actually the name of the method
    [request setHTTPBody:[@"" dataUsingEncoding:NSUTF8StringEncoding]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [client registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Success Print the response body in text
        NSString *resp = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        self.dictCakeDetails = [resp JSONValue];
        NSLog(@"%@", self.dictCakeDetails);
        
        [self loadProductDetails];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //Something Went wrong..
        NSLog(@"Error: %@", error);
    }];
    [operation start];
}

-(void)loadProductDetails{
    self.imgVwCoverCake.imageURL = [NSURL URLWithString:[[[self.dictCakeDetails objectForKey:@"images"] objectAtIndex:0] objectForKey:@"src"]];
    
    self.imgVwSmall1.layer.borderWidth = 1.0;
    self.imgVwSmall1.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgVwSmall1.layer.cornerRadius = 5.0;
    
    self.imgVwSmall2.layer.borderWidth = 1.0;
    self.imgVwSmall2.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgVwSmall2.layer.cornerRadius = 5.0;
    
    self.imgVwSmall3.layer.borderWidth = 1.0;
    self.imgVwSmall3.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgVwSmall3.layer.cornerRadius = 5.0;
    
    self.imgVwSmall4.layer.borderWidth = 1.0;
    self.imgVwSmall4.layer.borderColor = [UIColor clearColor].CGColor;
    self.imgVwSmall4.layer.cornerRadius = 5.0;
    
    self.imgVwSmall1.imageURL = [NSURL URLWithString:[[[self.dictCakeDetails objectForKey:@"images"] objectAtIndex:0] objectForKey:@"src"]];
    self.imgVwSmall2.imageURL = [NSURL URLWithString:[[[self.dictCakeDetails objectForKey:@"images"] objectAtIndex:1] objectForKey:@"src"]];
    self.imgVwSmall3.imageURL = [NSURL URLWithString:[[[self.dictCakeDetails objectForKey:@"images"] objectAtIndex:2] objectForKey:@"src"]];
    self.imgVwSmall4.imageURL = [NSURL URLWithString:[[[self.dictCakeDetails objectForKey:@"images"] objectAtIndex:3] objectForKey:@"src"]];
    
    self.lblCakeName.text = [self.dictCakeDetails objectForKey:@"name"];
    
    self.lblRating.text = [self.dictCakeDetails objectForKey:@"average_rating"];
    self.lblRating.text = [self.lblRating.text stringByAppendingString:@" *"];
    
    self.lblReviewRating.text = [NSString stringWithFormat:@"%@",[self.dictCakeDetails objectForKey:@"rating_count"]];
    self.lblReviewRating.text = [self.lblReviewRating.text stringByAppendingString:@" rating & "];
    self.lblReviewRating.text = [self.lblReviewRating.text stringByAppendingString:[NSString stringWithFormat:@"%@",[self.dictCakeDetails objectForKey:@"product_reviews_count"]]];
    self.lblReviewRating.text = [self.lblReviewRating.text stringByAppendingString:@" review"];
    
    if([self.dictCakeDetails objectForKey:@"stock_quantity"]>0){
        self.lblTextAvaOrNotAva.text = @"AVAILABLE";
        self.lblTextAvaOrNotAva.textColor = [UIColor greenColor];
        self.lblCircleAva.backgroundColor = [UIColor greenColor];
    }
    else{
        self.lblTextAvaOrNotAva.text = @"NOT AVAILABLE";
        self.lblTextAvaOrNotAva.textColor = [UIColor colorWithRed:242.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
        self.lblCircleAva.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:1.0];
    }
    
//    mainImageView = [[UIImageView alloc]init];
//    NSArray *animationArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"first.png"],[UIImage imageNamed:@"second.png"],[UIImage imageNamed:@"third.png"],[UIImage imageNamed:@"fourth.png"],[UIImage imageNamed:@"fifth.png"],[UIImage imageNamed:@"sixth.png"],[UIImage imageNamed:@"seventh.png"],[UIImage imageNamed:@"eight.png"],[UIImage imageNamed:@"nine.png"],[UIImage imageNamed:@"ten.png"], nil]; //add your images here
//    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(crossfade) userInfo:nil repeats:YES];
//    [mainImageView  setFrame:CGRectMake(50,50,100,100)];
//    mainImageView.animationImages = animationArray; //mainImageView is   imageview
//    mainImageView.animationDuration = 10;
//    mainImageView.animationRepeatCount = 0;
//    [mainImageView startAnimating];
//
//
//    [self.view addSubview:mainImageView];
    
}

//- (void)crossfade {
//    [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        mainImageView.alpha = !mainImageView.alpha;
//    }completion:^(BOOL done){
//        //
//    }];
//}

- (IBAction)clickBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
